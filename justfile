set windows-shell := ["powershell.exe", "-NoLogo", "-Command"]

default:
    cargo build --features bevy/dynamic_linking
    cargo run --features bevy/dynamic_linking -q

release:
    cargo build -r
