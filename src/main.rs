use std::f32::consts::{FRAC_PI_6, PI};

use bevy::{ecs::query::QueryFilter, prelude::*};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        //
        .add_systems(Startup, spawn_card)
        .add_systems(Startup, spawn_camera)
        .add_systems(Startup, spawn_light)
        //
        .add_systems(FixedUpdate, rotate_marker)
        // finish & run
        .run();
}

#[derive(Component, QueryFilter)]
pub struct Marker;

fn spawn_card(mut commands: Commands, asset_server: Res<AssetServer>) {
    let path = "Card/Card_Base.gltf#Scene0";
    let card_asset = asset_server.load(path);

    commands.spawn((
        Marker,
        SceneBundle {
            scene: card_asset,
            transform: Transform::from_xyz(0.0, 0.0, 0.0)
                .with_rotation(Quat::from_rotation_x(FRAC_PI_6)),
            ..Default::default()
        },
    ));
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(-10.0, 5.0, -10.0).looking_at(
            Vec3 {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            Vec3::Y,
        ),
        ..Default::default()
    });
}

fn spawn_light(mut commands: Commands) {
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            color: Color::ORANGE,
            ..Default::default()
        },
        transform: Transform::from_rotation(Quat::from_rotation_x(-PI-0.5)),
        ..Default::default()
    });
}

fn rotate_marker(mut query: Query<&mut Transform, With<Marker>>, time: Res<Time>) {
    for mut trans in query.iter_mut() {
        let angle = time.elapsed_seconds_wrapped();
        *trans = Transform::from_xyz(0.0, 0.0, 0.0).with_rotation(Quat::from_rotation_x(FRAC_PI_6));
        trans.rotate_local_y(angle);
    }
}
